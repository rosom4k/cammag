class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :name, unique: true, null: false
      t.string :ean, unique: true
      t.string :owner, default: ''
      t.string :description
      t.json :features, default: {}
      t.integer :status, default: 0

      # FK
      t.integer :item_type_id
      t.integer :item_group_id

      t.timestamps
    end

    create_table :histories do |t|
      t.integer :item_id, null: false
      t.string :value, null: false

      t.timestamps
    end

    add_foreign_key :items, :histories
  end
end
