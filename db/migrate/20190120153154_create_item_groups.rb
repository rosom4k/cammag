class CreateItemGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :item_groups do |t|
      t.string :name, null: false
      t.timestamps
    end

    add_foreign_key :items, :item_type_groups
  end
end
