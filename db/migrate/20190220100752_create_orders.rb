class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :status, default: 0
      t.integer :group_ids
      t.string :description
      t.string :number
      t.string :name
      t.string :owner
      t.string :manager
      t.datetime :date_from, null: false
      t.datetime :date_to, null: false
      t.timestamps
    end

    create_table :items_orders do |t|
      t.integer :order_id
      t.integer :item_id
      t.timestamps
    end

    add_foreign_key :items, :items_orders
    add_foreign_key :orders, :items_orders

  end
end
