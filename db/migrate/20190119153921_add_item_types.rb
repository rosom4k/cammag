class AddItemTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :item_types do |t|
      t.string :name, null: false
      t.string :icon
      t.string :feature_keys
      t.timestamps
    end

    add_foreign_key :items, :item_types
  end
end
