def rl(n)
  (0...n).map { |_| rand(9).to_s }.join()
end

@ean = 0

def item_groups
  ['Zestaw reporterski',
   'Zestaw nagraniowy',
   'Zestaw wyjazdowy',
   'Zestaw studyjny']
end

def rd()
  q = %w(Nowy Stary Prototypowy Solidny Mierny)
  c = %w(Niebieski Turkusowy Zielony Biały Czerwony Czarny)
  e = ['Lekkie zadrapania na obudowie', 'Uszkodzona elektronika', 'Wydziela nieprzyjemny zapach w dużych temperaturach', 'Kolor nie adekwatny do opisu', 'Oprogramowanie wymaga aktualizacji', 'Większe niż podane w opisie']

  q.shuffle.first + ' model. ' + c.shuffle.first.capitalize + ' kolor. ' + e.shuffle.first + '.'
end

def rand_order(date_from, date_to)
  order = Order.new(
         name: %w(Wyjazd-integracyjny Sport Transmisja Fakty Wiadomości Uwaga! Dlaczego-ja? Zamówienie-alfanumeryczne Życie-menela Rozmowy-pod-mostem Rozmowy-w-tłoku Tele-auto-konferencja Nagranie-testowe Konferencja-testowa Program-na-żywo).shuffle.first,
         number: [rand(100).to_s, rand(100).to_s, (rand(25) + 65).chr + (rand(25) + 65).chr].shuffle.join('-'),
         owner: ['Iwona Górzyńska', 'Janusz Januszowski', 'Dżesika Szczodra-Łagodna', "Bogdan Ciupa", 'Magdalena Wąsko', 'Pan Józek', 'Andrzej Waleczny', 'Ilona Iwona', 'Grażyna Jeżyna', 'Anna Wanna', 'Zuzanna Odpływ-Przepływna', 'Włodek Dorodek'].shuffle.first,
         manager: ['Iwona Górzyńska', 'Janusz Januszowski', 'Dżesika Szczodra-Łagodna', "Bogdan Ciupa", 'Magdalena Wąsko', 'Pan Józek', 'Andrzej Waleczny', 'Ilona Iwona', 'Grażyna Jeżyna', 'Anna Wanna', 'Zuzanna Odpływ-Przepływna', 'Włodek Dorodek'].shuffle.first,
         date_from: date_from,
         date_to: date_to,
  )
  order
end
def rand_item(t)
  case t
    when 0
      {
        name: "Cam-" + rl(2) + '00',
        ean: 'CM' + (@ean += 1).to_s.rjust(6, '0'),
        item_type_id: 1,
        description: rd
      }
    when 1
      {
        name: "L-" + rl(1) + '00' + rl(3),
        ean: 'CM' + (@ean += 1).to_s.rjust(6, '0'),
        item_type_id: 2,
        description: rd
      }
    when 2
      {
        name: "W-" + rl(2) + '-W1R3',
        ean: 'CM' + (@ean += 1).to_s.rjust(6, '0'),
        item_type_id: 3,
        description: rd
      }
    when 3
      {
        name: "ST4-00" + rl(4),
        ean: 'CM' + (@ean += 1).to_s.rjust(6, '0'),
        item_type_id: 4,
        description: rd
      }
  end
end

puts 'Will print:'
puts 't for new item type,'
puts 'g for new item group,'
puts 'i for new item.'
puts 'o for new order.'
[
  ["fa fa-fw fa-camera", 'Kamera', 'Szerokość,Długość,Kolor'],
  ["fa fa-fw fa-binoculars", 'Obiektyw', 'Ogniskowa,Zoom'],
  ["fa fa-fw fa-plug", 'Okablowanie', 'Długość'],
  ["fa fa-fw fa-arrows-v", 'Statyw', 'Wysokość,Rodzaj'],
  ["fa fa-fw fa-times", 'Inne', '']
].each do |it|
  print 't' if ItemType.create(name: it[1], icon: it[0], feature_keys: it[2])
end

item_groups.each do |z|
  (rand(10) + 20).times do |i|
    ig = ItemGroup.new(name: [z, [z.split(' ').map { |q| q[0].upcase }.join(''), i.to_s].join('-')].join(' '))
    print 'g' if ig.save
    (0..3).each do |item_type|
      (rand(2) + 1).times do
        item = Item.new(rand_item(item_type))
        print 'i' if ig.items.push(item)
      end
    end
  end
end

Order.create(name: 'Dummy', number: '000', date_from: Time.new(0), date_to: Time.new(0))
(1..4).each do |date_range|
  (1..1).each do
    r = rand(date_range+1)
    o = rand_order(Time.now + (12 * r).hours, Time.now + (36 * r).hours)
    ids = ItemGroup.includes(:items).where.not( items: {status: [1,2,3]}).pluck(:id).uniq
    ItemGroup.find(ids.shuffle.first).items.each{|i| o.items.push(i)}

    #ItemGroup.where(status: ITEM_ACTIVE).all.limit(rand(20) + 5).each { |i| o.items.push(i) }
    print 'o' if o.save
  end
end

(1..3).each do |date_range|
  o = rand_order(Time.now - 4.days, Time.now + date_range.days)
  ids = ItemGroup.includes(:items).where.not( items: {status: [1,2,3]}).pluck(:id).uniq
  items = ItemGroup.find(ids.shuffle.first).items
  items.each{|i| o.items.push(i)}
  print 'o' if o.save
  o.update(status: 1)
  items.each { |i| i.update(status: ITEM_BUSY) }
end


(1..2).each do
  o = rand_order(Time.now - 2.days, Time.now - 1.days)
  ids = ItemGroup.includes(:items).where.not( items: {status: [1,2,3]}).pluck(:id).uniq
  items = ItemGroup.find(ids.shuffle.first).items
  items.each{|i| o.items.push(i)}
  print 'o' if o.save
  o.update(status: 1)
  items.each { |i| i.update(status: ITEM_BUSY) }
end

(1..2).each do |q|
  ids = ItemGroup.includes(:items).where.not( items: {status: [1,2,3]}).pluck(:id).uniq
  items = ItemGroup.find(ids.shuffle.first).items
  dates = (-4..4).map{|i| Time.now - (14*i*q).hours}.reverse
  (1..4).each do |i|
    o = rand_order(dates[i], dates[i+1])
    items.each{|i| o.items.push(i)}
    print 'o' if o.save
    o.update(status: 1)
    items.each { |i| i.update(status: ITEM_BUSY) }
  end
end

items_active = Item.where(status: ITEM_ACTIVE).pluck(:id)
items_active_count = items_active.count

(items_active_count * 0.08).to_i.times do
  Item.find(items_active.shuffle.first).update(status: ITEM_SERVICE)
end

puts ''
puts "Created #{Item.count + ItemGroup.count + ItemType.count} records."