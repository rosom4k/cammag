require 'em-websocket'
require 'json'

EventMachine.run {
  @channel = EM::Channel.new

  EventMachine::WebSocket.start(:host => "0.0.0.0", :port => 8080, :debug => true) do |ws|

    ws.onopen {
      sid = @channel.subscribe { |msg| ws.send msg }
      @channel.push "<#{sid}> connected!"

      ws.onmessage { |msg|
        @channel.push "<#{sid}>: #{msg}"
      }

      ws.onclose {
        @channel.push "<#{sid}> disconnected!"
        @channel.unsubscribe(sid)
      }
    }

  end

  puts "Server started"
}
