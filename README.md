# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version 
`2.5.1`, or `2.5.3`
* System dependencies 
`rvm`, `nodejs`
* Configuration
`---`
* Database creation (sqlite3)
`rake db:migrate`
* Database initialization
`rake db:seed`
* How to run the test suite
`---`
* Services (job queues, cache servers, search engines, etc.)
`none`
* Deployment instructions
`rails server`
* ...
