module ItemsHelper
  def audited_item_header v
    case v
    when 'item_type_id'
      'Typ sprzętu'
    when 'item_group_id'
      'Grupa'
    when 'name'
      'Nazwa'
    when 'ean'
      'EAN'
    when 'description'
      'Opis'
    when 'features'
      'Cechy'
    when 'owner'
      'Osoba'
    else
      v.capitalize
    end
  end

  def audited_item_value k, v
    case k
    when 'item_type_id'
      ItemType.find(v).label rescue '-'
    when 'item_group_id'
      ItemGroup.find(v).name rescue '-'
    when 'status'
      case v
      when ITEM_ACTIVE
        '<span class="label label-success"><i class="fa fa-fw fa-check" aria-hidden="true"></i> Na stanie</span>'
      when ITEM_RESERVED
        '<span class="label label-warning"><i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i> Rezerwacja</span>'
      when ITEM_BUSY
        '<span class="label label-primary"><i class="fa fa-circle-o-notch fa-fw"></i> Zajęty</span>'
      when ITEM_SERVICE
        '<span class="label label-danger"><i class="fa fa-fw fa-times" aria-hidden="true"></i> Serwis</span>'
      else 
        '-'
      end 
    when 'features'
      v.map{|k, v| "#{k}: #{v}"}.join(", ")
    when 'owner'
      v.empty? ? '(brak)' : v
    else
      v
    end
  end

  def recent_service_date audits
    audits.select{|q| 
      q.audited_changes.keys.include?("status") && q.audited_changes[:status].is_a?(Array) && q.audited_changes[:status].last == 3
    }.map{|q| q.created_at}.max
  end

end
