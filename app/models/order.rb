class Order < ApplicationRecord

  has_and_belongs_to_many :items, through: :orders_items
  after_create :set_item_statuses_to_reserved

  def set_item_statuses_to_reserved
    items.each do |item|
      item.update(status: ITEM_RESERVED)
    end
    end

  def barcode
    require 'barby/barcode/code_128'
    require 'barby/outputter/cairo_outputter'

    begin
      barcode = Barby::Code128.new(number.rjust(8, '0'))
      outputter = Barby::CairoOutputter.new(barcode)
      outputter.to_svg(height: 35, ydim: 18, margin: 0)
    rescue
      "<span>---</span>"
    end
  end


end


