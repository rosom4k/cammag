class ItemGroup < ApplicationRecord
  has_many :items, dependent: :nullify

  def unavailable?
    i = items.pluck(:status)
    i.include?(ITEM_RESERVED) || i.include?(ITEM_BUSY) || i.include?(ITEM_SERVICE)
  end

end
