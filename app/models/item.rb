class Item < ApplicationRecord
  audited

  validates_uniqueness_of :ean
  validates_uniqueness_of :name
  
  belongs_to :item_type
  has_many :histories
  belongs_to :item_group, optional: true
  has_and_belongs_to_many :orders, through: :orders_items

  def label
    ('<i class="' + item_type.icon + '" ></i> ' + name).html_safe
  end

  def group_name
    if item_group.nil?
      '-'
    else
      item_group.name
    end
  end

  def barcode
    require 'barby/barcode/code_128'
    require 'barby/outputter/cairo_outputter'

    begin
      barcode = Barby::Code128.new(ean)
      outputter = Barby::CairoOutputter.new(barcode)
      outputter.to_svg(height: 12, ydim: 10, margin: 0)
    rescue
      "<span>---</span>"
    end
  end
end
