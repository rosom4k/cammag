class ItemType < ApplicationRecord
  has_many :items, dependent: :delete_all

  def label
    ('<i class="' + icon + '" aria-hidden="true"></i> ' + name).html_safe
  end

  def fa
    ('<i class="' + icon + '" aria-hidden="true"></i>').html_safe
  end

  def f_keys
    if feature_keys
      feature_keys.split(',')
    else
      []
    end
  end
end

