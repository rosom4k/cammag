class HomeController < ApplicationController

  def index
    @item_count       = Item.count
    @item_type_count  = ItemType.count
    @item_group_count = ItemGroup.count 
    @order_count      = Order.count
    @service_items    = Item.where(status: ITEM_SERVICE).includes(:item_type).includes(:audits)
    @orders           = Order.where.not(status: ORDER_DONE, id: 1).order(:date_to)
    @from             = -3
    @to               = 7
    @groups           = schedule_groups
  end

  def scan
  end

  def schedule_pdf
    @from   = params[:from].to_i - Time.now.wday + 1
    @to     = params[:to].to_i - Time.now.wday
    @groups = schedule_groups
    render pdf: ["Harmonogram", Time.now.strftime("%d-%m")].join('-'), 
           encoding: 'utf8',
           orientation: 'Landscape',
           background: true,
           no_background: false,
           grayscale: false,
           lowquality: false
  end
  private

  def schedule_groups
  	groups = {}
    item_groups_ending = ItemGroup.includes(:items).includes(:items => [ :orders ])\
      .where(items: {status: [ITEM_BUSY, ITEM_RESERVED]})\
      .where(orders: {date_to: Time.now + @from.days .. Time.now + @to.days})
    item_groups_starting = ItemGroup.includes(:items).includes(:items => [ :orders ])\
      .where(items: {status: [ITEM_BUSY, ITEM_RESERVED]})\
      .where(orders: {date_from: Time.now + @from.days .. Time.now + @to.days})
    (item_groups_ending + item_groups_starting).each do |item_group|
      # gather order information here
      groups[item_group.name] = {id: item_group.id, orders: []} if groups[item_group.name].nil?

      recent_orders = item_group.items.map do |item|
        groups[item_group.name][:orders] << item.orders
      end
      groups[item_group.name][:orders].flatten!.uniq!
      
    end
    return groups
  end
end
