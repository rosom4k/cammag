class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]

  # GET /items
  def index
    @items = Item.includes(:item_type).includes(:item_group)
  end

  # GET /item_barcodes
  def index_barcodes
    @items = Item.all
  end

  # GET /items/1
  def show
  end

  # GET /items/new
  def new
    @item = Item.new
  end

  # GET /items/1/edit
  def edit
  end

  def add_history
    @item = Item.find(params[:item_id])
    if @item.histories.push(History.new(value: params[:history][:value]))
      redirect_to @item, notice: 'Wpis dodany.'
    else
      render :show, alert: 'Coś poszło nie tak.'
    end
  end

  # GET /items/1/ajax
  def ajax
    if params[:back]
      @item = Item.where(ean: params[:ean], status: [ITEM_BUSY, ITEM_SERVICE]).first
      if @item
        item_order = @item.orders.order(:created_at).last
      end
    else
      @item = Item.where(ean: params[:ean], status: [ITEM_ACTIVE, ITEM_RESERVED]).first
      if @item
        @order = Order.find(params[:order])
        if @item.status == ITEM_RESERVED && !@item.orders.include?(@order)
          # przedmiot zarezerwowany w innym zleceniu
          @item = nil
          render layout: false
          return
        end
        item_order = @item.orders.order(:created_at).last
      end
    end
    render layout: false
  end

  # POST /items
  def create
    @item = Item.new(item_params)

    if @item.save
      redirect_to @item, notice: 'Item was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /items/1
  def update
    @item.assign_attributes(item_params)

    features = {}
    @item.item_type.f_keys.each_with_index do |k, i|
      features[k] = params[:item][:features][i]
    end
    @item.features = features

    if @item.save
      redirect_to @item, notice: 'Item was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /items/1
  def destroy
    @item.destroy
    redirect_to items_url, notice: 'Item was successfully destroyed.'
  end

  def change_status
    ids = params[:ids].split(',')
    if ids.blank?
      render json: { status: 'error', msg: 'Nie wybrano sprzętu.' }
      return
    end

    statuses = Item.find(ids).pluck(:status).uniq
    unless (statuses & [ITEM_RESERVED, ITEM_BUSY, ITEM_SERVICE]).blank?
      render json: { status: 'warning', msg: 'Nie wyporzyczono, nie cały sprzęt jest na stanie.' }
      return
    end

    Item.find(ids).each do |item|
      item.update(status: ITEM_BUSY)
    end
    render json: { status: 'success', msg: 'Wyporzyczono sprzęt.' }

  end

  def service
    @item = Item.find(params[:item_id]) rescue nil

    unless @item
      redirect_to items_url, error: 'Przedmiotu nie odnaleziono.' and return
    end

    if [ITEM_ACTIVE, ITEM_RESERVED].include? @item.status
      @item.update(status: ITEM_SERVICE)
      redirect_to @item, notice: 'Przekazano do serwisu.'
    else
      redirect_to @item, flash: { error: 'Nie można skierować do serwisu. Sprawdź status przedmiotu.' }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_item
    @item = Item.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def item_params
    params.require(:item).permit(:name, :item_type_id, :ean, :description)
  end
end
