class ItemGroupsController < ApplicationController
  before_action :set_item_group, only: [:show, :edit, :update, :destroy]

  # GET /item_groups
  def index
    @item_groups = ItemGroup.includes(:items => :item_type)
  end

  # GET /item_groups/1
  def show
  end

  # GET /item_groups/new
  def new
    @item_group = ItemGroup.new
  end

  # GET /item_groups/1/edit
  def edit
    redirect_back(fallback_location: @item_group) if @item_group.unavailable?
  end

  # POST /item_groups
  def create
    @item_group = ItemGroup.new(item_group_params)

    if @item_group.save
      redirect_to item_groups_url, notice: 'Item group was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /item_groups/1
  def update
    @item_group.assign_attributes(item_group_params)
    @item_group.item_ids = params['item_group']['item_ids']
    if @item_group.save#update(item_group_params)
      redirect_to item_groups_url, notice: 'Item group was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /item_groups/1
  def destroy
    @item_group.item_ids.clear
    @item_group.save
    @item_group.destroy
    redirect_to item_groups_url, notice: 'Item group was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item_group
      @item_group = ItemGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def item_group_params
      params.require(:item_group).permit(:name, item_ids: {})
    end
end
